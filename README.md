# LIS4368 - Advanced Mobile Web Application Development

## Patricio Gonzalez De Torres 
**LIS4368 Requirements:**

Course Work Links:

1. [A1 README.md](a1/README.md)
      - Install Tomcat
      - Install JDK
      - Provide Screenshots of installations
      - Create Bitbucket repo
      - Complete Bitbucket tutorial(bitbucketstationslocations and myteamquotes)
      - Provide git command descriptions
2. [A2 README.md](a2/README.md)
      - Assessment Links
      - Screenshots
      - Chapter Questions

3. [A3 README.md](a3/README.md)
      - ERD 
      - Include data (10 records in each table)
      - Provide Bitbucket accessto repo
      - Skill sets 4-6

4. [P1 README.md](p1/README.md)
      - Complete client side validation 
      - Working Carousel with links
      - Skillset 7
      - Skillset 8
      - Skillset 9

5. [A4 README.md](a4/README.md)
      - Server-side data validation 
      - MVC Framework 
      - SKillsets

6. [A5 README.md](a5/README.md)
      - Batabase Entry
      - MVC Framework 
      - SKillsets
      
7.  [P2 README.md](p2/README.md)
      - Batabase Entry
      - MVC Framework 
      - Modify and delete functionality 