## LIS4368

## Patricio Gonzalez De Torres

### Assignment 3 Requirements:

1. Assessment Links
2. Screenshots
3. Chapter Questions

#### README.md file should include the following items:

## Assessment Links ##
a. http://localhost:9999/LIS4368/a3

b. http://localhost:9999/

c. [A3 MWB](docs/a3.mwb)

d. [A3 SQL](docs/a3_new.sql)


#### Assignment Screenshots:

*Screenshot of ERD*

![A3 ERD](img/a3.png)

*Screenshot of Index*:

![Index.jsp](img/index.png)




#### Skill Sets:
 
*Screenshot of SS4*

![SS1](img/ss4.png)

*Screenshot of SS5*

![SS1](img/ss5.png)

*Screenshot of SS6*

![SS1](img/ss6.png)
