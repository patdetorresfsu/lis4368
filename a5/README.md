## LIS4368

## Patricio Gonzalez De Torres

### Assignment 5 Requirements:

1. Assessment Links
2. Screenshots
3. Chapter Questions

#### README.md file should include the following items:

## Assessment Links ##
a. http://localhost:9999/LIS4368/customerform.jsp?assign_num=a5

b. http://localhost:9999/


#### Assignment Screenshots:

*Screenshot of Pass Validation*:

![Pass](img/validentry.png)

*Screenshot of Thank you page*:

![Thanks](img/thanks.png)

*Screenshot of Database Entry page*:

![Thanks](img/databaseentry.png)




#### Skill Sets:
 
*Screenshot of SS13*

![SS1](img/ss13.png)

*Screenshot of SS14*

![SS1](img/ss14.png)

