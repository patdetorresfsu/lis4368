## LIS4363

## Patricio Gonzalez De Torres

### Assignment 1 Requirements:

1. Distributed Version Control with Git and Bitbucket
2. Java/Tomcat Development Installation
3. Chapter Questions

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running http://localhost:9999 
* git commands
* Bitbucket repo links

> #### Git commands w/short descriptions:

1. git init - creates an empty git repository
2. git status - Displays paths tat have differeces between the index file and the current head commit
3. git add - Updates te index using the current content found in the working tree
4. git commit - Creates a new commit containing the current contents of the index and the given log message describing the changes
5. git push - Updates remote refs using local refs
6. git pull - INcorporates changes from a remote repository into the current branch
7. git fetch - fetches branches and tags from one or more other repositories

#### Assignment Screenshots:

*Screenshot of Java Hello http://localhost*:

![Hello](img/hello.png)

*Screenshot of Tomcat localhost:9999*:

![Tomcat](img/9999.png)

*Screenshot of Index*:

![Index](img/index.png)

*Screenshot of A1*:

![A1](img/a1.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
