## LIS4368

## Patricio Gonzalez De Torres

### Porject 2 Requirements:

1. Assessment Links
2. Screenshots
3. Chapter Questions

#### README.md file should include the following items:

## Assessment Links ##
a. http://localhost:9999/LIS4368/customerform.jsp?assign_num=p2

b. http://localhost:9999/


#### Assignment Screenshots:

*Screenshot of Valid User Form Entry*:

![Pass](img/customerform.jsp.png)

*Screenshot of Thank you page*:

![Thanks](img/thanks.png)

*Screenshot of Customers Display page*:

![customers](img/customers.png)

*Screenshot of modify page*:

![modify](img/modify.png)

*Screenshot of Modified data page*:

![modified data](img/modified.png)

*Screenshot of Delete warning page*:

![delete](img/deletewarning.png)

*Screenshot of Database changes page*:

![database](img/databasechanges.png)



