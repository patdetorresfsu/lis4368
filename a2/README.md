## LIS4363

## Patricio Gonzalez De Torres

### Assignment 2 Requirements:

1. Assessment Links
2. Screenshots
3. Chapter Questions

#### README.md file should include the following items:

## Assessment Links ##
a. http://localhost:9999/hello 

b. http://localhost:9999/hello/HelloHome.html

c. http://localhost:9999/hello/sayhello

d. http://localhost:9999/hello/querybook.html


#### Assignment Screenshots:

*Screenshot of Java Hello http://localhost:9999/hello*

![Hello](img/hello.png)

*Screenshot of QueryBook*:

![QueryBook](img/querybook.png)

*Screenshot of Query Results*:

![Index](img/queryresults.png)

*Screenshot of sayhello*:

![Index](img/sayhello.png)

*Screenshot of A2*:

![A1](img/a2.png)


#### Skill Sets:
 
*Screenshot of SS1*

![SS1](img/ss1.png)

*Screenshot of SS2*

![SS1](img/ss2.png)

*Screenshot of SS3*

![SS1](img/ss3.png)
