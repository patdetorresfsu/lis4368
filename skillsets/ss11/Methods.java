import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

public class Methods {

    public static void getRequirements(){
        System.out.println("Developer: Patricio Gonzalez De Torres");
        System.out.println("");
    }

    public static void getInterestValues() {
        Scanner sc = new Scanner(System.in);
        double principal = 0.0;
        double rate = 0.0;
        int time = 0; //years

        System.out.print("Current principal: $");
        while (!sc.hasNextDouble()){
            System.out.println("Not valid number");
            sc.next();
            System.out.print("Please try again. Enter principal: $");
        }
        principal = sc.nextDouble();

        System.out.print("Interest rate (per year): ");    
        while (!sc.hasNextDouble()){
            System.out.println("Not valid number");
            sc.next();
            System.out.print("Please try again. Enter interest rate: ");
        }
        rate = sc.nextDouble();
        rate = rate / 100;

        System.out.print("Total Time in years: ");
        while (!sc.hasNextDouble()){
            System.out.println("Not valid number");
            sc.next();
            System.out.print("Please try again. Enter total time in years: ");
        }
        time = sc.nextInt();
        sc.close();
        calculateInterest(principal, rate, time);
    }
    
    public static void calculateInterest(double p, double r, int t){
        int n = 12;
        double i = 0.0;
        double a = 0.0;

        a = p * Math.pow(1 + (r / n), n * t);
        i = a - p;

        r = r * 100;

        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
        System.out.println("Initial principal: " + nf.format(p));
        System.out.printf("Annual interest rate: %.1f%%%n", r);
        System.out.println("Total monthly compound interest after " + t + " years " + nf.format(i));
        System.out.println("Total amount: " + nf.format(a));

    }
}