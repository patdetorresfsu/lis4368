
import java.util.Properties;
import java.util.Scanner;

public class Methods {

    public static void getRequirements(){
        System.out.println("Developer: Patricio Gonzalez De Torres");
        System.out.println();
    }
    public static void lineAnalysis() {

        Scanner sc = new Scanner(System.in);
        
        String userLine = "";
        char userChar = ' ';
        int charCount = 0;

        System.out.print("Enter line of text: ");
        while(!sc.hasNextLine()){
            System.out.println("Invalid input");
            sc.nextLine();
            System.out.println("PLease try again, enter a line of text: ");
        }
        userLine = sc.nextLine();

        System.out.print("Enter charcter to check: ");
        while(!sc.hasNextLine()){
            System.out.println("Invalid input");
            sc.nextLine();
            System.out.println("PLease try again, enter a line of text: ");
        }
        userChar = sc.next().charAt(0);

        for(int i = 0; i < userLine.length(); i++){
            if(userLine.charAt(i) == userChar){
                charCount++;
            }
        }

        System.out.println("Number of charcters in line of text: " + userLine.length());
        System.out.println("The charcter " + userChar + " appears " + charCount + " time(s) in the line of text.");
        System.out.println("ASCII value: " + (int) userChar);
    }
}