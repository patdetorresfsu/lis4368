
public class Methods {
    
    public static void getRequirements() {
        System.out.println("Developer: Patricio Gonzalez De Torres");
        System.out.println("Program loops through array of Floats");
        System.out.println("Use following values: 1.0, 2.1, 3.2, 4.3, 5.4");
        System.out.println("Use following loop structures: for, enhanced for, while, do... while");
        System.out.println("\nNote: Preset loops: for, enhanced for, while. Posttest loop: do... while");
    }

    public static void arrayLoops() {
        //String numbers[] = {"1.0", "2.1", "3.2", "4.3", "5.4"};
        
        float numbers[] ={1.0f, 2.1f, 3.2f, 4.3f, 5.4f};

        System.out.println("For Loop: ");
        for (int i = 0; i < numbers.length; i++){
            System.out.println(numbers[i]);
        }

        System.out.println("\nEnhanced for loop: ");
        for(float test : numbers){
            System.out.println(test);
        }

        System.out.println("\nWhile Loop: ");
        int i = 0;
        while (i < numbers.length){
            System.out.println(numbers[i]);
            i++;
        }

        i = 0;
        System.out.println("\ndo... while loop: ");
        do{
            System.out.println(numbers[i]);
            i++;
        }
        while(i < numbers.length);
        
    }

    


}


