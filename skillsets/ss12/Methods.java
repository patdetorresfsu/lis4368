import java.util.Scanner;

public class Methods {

    public static void getRequirements(){
        System.out.println("Developer: Patricio Gonzalez De Torres");
        System.out.println("");
    }
    public static String[] createArray(){
        Scanner sc = new Scanner(System.in);
        int arraySize = 0;
        System.out.print("How many favorite programming languages do you have (min 1): ");
        while (!sc.hasNextInt()){
            System.out.println("Not valid number");
            sc.next();
            System.out.print("Please try again. Enter valid input (min 1): ");
        }
        arraySize = sc.nextInt();

        while(arraySize < 1){
            System.out.print("\n List must be greater than 0. Please enter number of favorite programming languages: ");
            while (!sc.hasNextInt()){
                System.out.println("Not valid number");
                sc.next();
                System.out.print("Please try again. Enter valid input (min 1): ");
            }
            arraySize = sc.nextInt();
        }

        String yourArray[] = new String[arraySize];
        return yourArray;
    }

    public static void copyArray(String [] str1){
        Scanner sc = new Scanner(System.in);
        System.out.println("PLease enter your " + str1.length + " Favorite programming langauges: ");
        for (int i = 0; i < str1.length; i++){
            str1[i] = sc.nextLine();
        }

        String str2[] = new String[str1.length];

        int myCounter = 0;
        for(String myIterator: str1){
            str2[myCounter++] = myIterator;
        }
        System.out.println();

        System.out.println("Printing str1 array: ");
        for (String myIterator: str1){
            System.out.println(myIterator);
               }
               System.out.println("Printing str2 array: ");
         for (String myIterator: str2){
           System.out.println(myIterator);
                          }
        System.out.println();
        sc.close();
    }
}