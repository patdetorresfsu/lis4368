class Car extends Vehicle {
    private float speed;
    
    public Car()
    {
        super();
        System.out.println("Inside car default contructor.");
        speed = 100;
    }

    public Car(String m, String d, int y, float s)
    {
       super(m,d,y);
       System.out.println("Inside car constructor with parameters.");
       speed = s; 
    }

    public double setSpeed()
    {
        return speed;
    }

    public void setSpeed(float s)
    {
        speed = s;
    }

    public void print()
    {
        super.print();
        System.out.println(", Speed " + speed);
    }
}
