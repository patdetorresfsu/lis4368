import java.util.Scanner;

import javax.swing.plaf.synth.SynthOptionPaneUI;

class VehicleDemo{
    public static void main(String[] args)
    {
        String mk = "";
        String md = "";
        int yr = 0;
        float sp = 0.0f;
        Scanner sc = new Scanner(System.in);

        System.out.print("\n /////Below are class default constructor values:////");
        Vehicle v1 = new Vehicle();
        System.out.println("\n Make = " + v1.getMake());
        System.out.println("Model = " + v1.getModel());
        System.out.println("Year = " + v1.getYear());

        System.out.print("Below are base class user enetered values:");

        System.out.print("\nMake: ");
        mk = sc.nextLine();
        System.out.print("Model: ");
        md = sc.nextLine();
        System.out.print("Year: ");
        yr = sc.nextInt();

        Vehicle v2 = new Vehicle(mk,md,yr);
        System.out.println("\n Make = " + v2.getMake());
        System.out.println("Model = " + v2.getModel());
        System.out.println("Year = " + v2.getYear());

        System.out.println("Below using setter methods to pass literal values, then print() method to display values");

        Car c1 = new Car();
        System.out.println("Make = " + c1.getMake());
        System.out.println("Model = " + c1.getModel());
        System.out.println("Year = " + c1.getYear());
        System.out.println("Speed = " + c1.getSpeed());

        System.out.println("Or...");
        c1.print();

        System.out.println("Below are derived class user entered values.");

        System.out.println("Make: ");
        mk = sc.next();
        System.out.println("Model: ");
        md = sc.next();
        System.out.println("Year: ");
        yr = sc.nextInt();
        System.out.println("Speed: ");
        sp = sc.nextFloat();
        
        Car c2 = new Car(mk, md, yr, sp);
        System.out.println("Make = " + c1.getMake());
        System.out.println("Model = " + c1.getModel());
        System.out.println("Year = " + c1.getYear());
        System.out.println("Speed = " + c1.getSpeed());

        System.out.println("Or...");
        c2.print();
        
    }
}
