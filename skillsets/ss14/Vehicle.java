class Vehicle {
    private String make;
    private String model;
    private int year;


    public Vehicle()
    {
        make = "My Make";
        model = "My Model";
        year = 1970;
    }

    public Vehicle(String make, String model, int year)
    {
        System.out.println("Inside vehicle contructor with parameters.");
        this.make = make;
        this.model = model;
        this.year = year;
    }

    public String getMake()
    {
        return make;
    }

    public void setMake(String mk)
    {
        make = mk;
    }

    public String getModel()
    {
        return model;
    }

    public void setModel(String md)
    {
        model = md;
    }

    public int getYear()
    {
        return year;
    }

    public void setYear(int yr)
    {
        year = yr;
    }

    public void print()
    {
        System.out.println("Make: " + make + ", Model " + model + ", Year: " + year);
    }
}

