
import java.util.Properties;
import java.util.Scanner;

import javax.swing.plaf.synth.SynthSpinnerUI;

public class Methods {

    public static void getRequirements(){
        System.out.println("Developer: Patricio Gonzalez De Torres");
        System.out.println();
    }
    public static void determineChar() {

       char ch = ' ';
       char chTest = ' ';
       Scanner sc = new Scanner(System.in);

       System.out.print("Please enter a charcter: ");

       ch = sc.next().charAt(0);
       chTest = Character.toLowerCase(ch);

       if(chTest == 'a' || chTest == 'e' || chTest == 'i' || chTest == 'o' || chTest == 'u' || chTest == 'y'){
           System.out.println(ch + " is a vowel. ASCII value: " + (int) ch);
       }
       else if(ch >= '0' && ch <= '9'){
           System.out.println(ch + " is an int. ASCII Value: " + (int) ch);
       }
       else if((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'a')){
        System.out.println(ch + " is a consonant. ASCII Value: " + (int) ch);
       }
       else 
       {
        System.out.println(ch + " is a special charcter. ASCII Value: " + (int) ch);
       }
       sc.close();
    }
}