import java.util.Scanner;

public class Methods {
 
    public static void getRequirements() {
        System.out.println("Developer: Patricio Gonzalez De Torres");
        System.out.println("Program swaps two user-entered floating-point values");
        System.out.println("Create at least two user-defined methods: getRequirements(), and numberSwap");
        System.out.println("Use following loop structures: for, enhanced for, while, do... while");
        System.out.println("\nNote: Preset loops: for, enhanced for, while. Posttest loop: do... while");
    }
public static void numberSwap(){
    Scanner sc = new Scanner(System.in);

    float num1, num2, temp = 0.0f;

    System.out.print("Enter num1: ");
    while(!sc.hasNextFloat()){
        System.out.println("invalid input\n");
        sc.next();
        System.out.println("Please try again. Enter num1: ");
    }
    num1 = sc.nextFloat();

    System.out.println("Enter num2: ");
    while(!sc.hasNextFloat()){
        System.out.println("invalid input\n");
        sc.next();
        System.out.println("Please try again. Enter num2: ");
    }
    num2 = sc.nextFloat();

    System.out.println("Before swap: ");
    System.out.println("num1: " + num1);
    System.out.println("num2: " + num2);
    
    temp = num1;
    num1 = num2;
    num2 = temp;

    System.out.println("After swap: ");
    System.out.println("num1: " + num1);
    System.out.println("num2: " + num2);

    sc.close();
    
}

}
