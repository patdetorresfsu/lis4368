import java.util.Scanner;

public class Methods {

    public static void getRequirements(){
        System.out.println("Developer: Patricio Gonzalez De Torres");
        System.out.println("");
    }

    public static void calculateNumbers(){
        double num1 = 0.0, num2 = 0.0;
        char operation = ' ';
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter mathematical operation (a, s, m, d, e):");

        operation = sc.next().toLowerCase().charAt(0);

        while (operation != 'a' && operation != 's' && operation != 'm' && operation != 'd' && operation != 'e') {
            System.out.print("\n Incorrect operation please enter correct operation.");
            operation = sc.next().toLowerCase().charAt(0);
        }

        System.out.println("PLease enter first number" );
        while(!sc.hasNextDouble()){
            System.out.println("Not valid number ");
            sc.next();
            System.out.println("PLease try again. Enter first number.");
        }
        num1 = sc.nextDouble();

        System.out.println("PLease enter second number: ");
        while (!sc.hasNextDouble()){
            System.out.println("Not valid number ");
            sc.next();
            System.out.println("Please enter vaid number");
        }
        num2 = sc.nextDouble();

        if(operation == 'a'){
            Addition(num1, num2);
        }
        else if(operation == 's'){
            Subtraction(num1, num2);
        }
        else if(operation == 'm'){
            Multiply(num1, num2);
        }
        else if(operation == 'd'){
            Divide(num1, num2);
        }
        if(operation == 'e'){
            Exponential(num1, num2);
        }
    }

    public static void Addition(double n1, double n2){
        System.out.print(n1 + " + " + n2 + " = ");
        System.out.printf("%.2f", (n1 + n2));
    }

    public static void Subtraction(double n1, double n2){
        System.out.print(n1 + " - " + n2 + " = ");
        System.out.printf("%.2f", (n1 - n2));
    }

    public static void Multiply(double n1, double n2){
        System.out.print(n1 + " * " + n2 + " = ");
        System.out.printf("%.2f", (n1 * n2));
    }

    public static void Divide(double n1, double n2){
        System.out.print(n1 + " / " + n2 + " = ");
        System.out.printf("%.2f", (n1 / n2));
    }

    public static void Exponential(double n1, double n2){
        System.out.print(n1 + " to the power of " + n2 + " = ");
        System.out.printf("%.2f", (Math.pow(n1, n2)));
    }
}