
import java.io.File;
import java.util.Properties;
import java.util.Scanner;

import javax.swing.plaf.synth.SynthSpinnerUI;
import javax.swing.plaf.synth.SynthStyleFactory;

public class Methods {

    public static void getRequirements(){
        System.out.println("Developer: Patricio Gonzalez De Torres");
        System.out.println("");
    }
    public static void directoryInfo() {

        String userDir = "";
        Scanner sc = new Scanner(System.in);

        System.out.print("PLease enter directory path: ");

        userDir = sc.nextLine();
        File directoryPath = new File(userDir);

        File fileList[] = directoryPath.listFiles();

        System.out.println("List files and directories in specified directory: ");
        for(File file : fileList){
            System.out.println("Name: " + file.getName());
            System.out.println("Path: " + file.getAbsolutePath());
            System.out.println("Size (Bytes): " + file.length());
            System.out.println("Size (KB): " + file.length()/(1024));
            System.out.println("Size (MB): " + file.length()/(1024*1024));
            System.out.println("");
        }
        sc.close();
    }
}