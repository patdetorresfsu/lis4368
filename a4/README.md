## LIS4368

## Patricio Gonzalez De Torres

### Assignment 4 Requirements:

1. Assessment Links
2. Screenshots
3. Chapter Questions

#### README.md file should include the following items:

## Assessment Links ##
a. http://localhost:9999/LIS4368/customerform.jsp?assign_num=a4

b. http://localhost:9999/


#### Assignment Screenshots:

*Screenshot of Failed Validation*:

![Failed](img/failedvalid.png)

*Screenshot of Thank you page*:

![Thanks](img/pass.png)




#### Skill Sets:
 
*Screenshot of SS10*

![SS1](img/ss10.png)

*Screenshot of SS11*

![SS1](img/ss11.png)

*Screenshot of SS12*

![SS1](img/ss12.png)
