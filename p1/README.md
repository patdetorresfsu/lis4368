# LIS4381

### Patricio Gonzalez De Torres

## Project 1 Requirements:

1. Complete client side validation 
2. Working Carousel with links
3. Skillset 7
4. Skillset 8
5. Skillset 9


#### README.md file should include the following items:

* Screenshot of main Web Page
* Screenshot of Skillset 7 running
* Screenshot of Skillset 8 running
* Screenshot of Skillset 9 running
* Screenshot of Successful Data Validation 
* Screenshot of Failed Data Validation  

## Assignment Screenshots:

### Project 1 Ckient side Validation  
*Screenshot of Pet Store Data Validation running http://localhost*:

*Main Web Page* 
![Front Page](img/frontpage.png)

*Failed Data Validation*:
![failed](img/failvalidation.png)

*Successful Data Validation*:
![Success](img/passvalidation.png)

*Skillset 7 *:
![Skillset 10](img/ss7.png)

*Skillset 8 *:
![Skillset 11](img/ss8.png)

*Skillset 9 *:
![Skillset 12](img/ss9.png)


