<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Patricio Gonzalez De Torres">
	<link rel="icon" href="favicon.ico">

	<title>LIS4368 - Project 1</title>
		<jsp:include page="../css/include_css.jsp" />
</head>
<body>

	<jsp:include page="../global/nav.jsp" />

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<jsp:include page="global/header.php" />
					</div>

					<h2>Customers</h2>

						<form id="defaultForm" method="post" class="form-horizontal" action="#">
								<div class="form-group">
										<label class="col-sm-4 control-label">FName:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="30" name="Fname" />
										</div>
								</div>
						
						<form id="defaultForm" method="post" class="form-horizontal" action="#">
								<div class="form-group">
										<label class="col-sm-4 control-label">LName:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="30" name="Lname" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Street:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="30" name="street" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">City:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="30" name="city" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">State:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="2" name="state" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Zip:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="9" name="zip" />
										</div>
								</div>
								<div class="form-group">
										<label class="col-sm-4 control-label">Phone:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="10" name="phone" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Email:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="100" name="email" />
										</div>
								</div>

								<div class="form-group">
									<label class="col-sm-4 control-label">Balance:</label>
									<div class="col-sm-4">
											<input type="text" class="form-control" maxlength="11" name="Balance" />
									</div>
							</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Total Sales:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="11" name="TotalSales" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Notes :</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="11" name="Notes" />
										</div>
								</div>

								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" name="add" value="Submit">Submit</button>
										</div>
								</div>
						</form>

			<jsp:include page="global/footer.php" />
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<jsp:include page="../js/include_js.jsp" />

<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
 $(document).ready(function() {

	$('#defaultForm').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					Fname: {
							validators: {
									notEmpty: {
									 message: 'First Name required'
									},
									stringLength: {
											min: 1,
											max: 30,
									 message: 'Name no more than 30 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[\w\-\s]+$/,
										message: 'First Name can only contain letters, numbers, hyphens, and underscore'
									},									
							},
					},
					Lname: {
							validators: {
									notEmpty: {
									 message: 'Last Name required'
									},
									stringLength: {
											min: 1,
											max: 30,
									 message: 'Last no more than 30 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[\w\-\s]+$/,
										message: 'Last Name can only contain letters, numbers, hyphens, and underscore'
									},									
							},
					},

					street: {
							validators: {
									notEmpty: {
											message: 'Street required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'Street no more than 30 characters'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[a-zA-Z0-9,\-\s\.]+$/,		
									message: 'Street can only contain letters, numbers, commas, hyphens, or periods'
									},									
							},
					},

					city: {
							validators: {
									notEmpty: {
									 message: 'City required'
									},
									stringLength: {
											min: 1,
											max: 30,
									 message: 'City no more than 30 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[a-zA-Z0-9\-\s]+$/,
										message: 'City can only contain letters, numbers, hyphens, and underscore'
									},									
							},
					},

					state: {
							validators: {
									notEmpty: {
									 message: 'State required'
									},
									stringLength: {
											min: 2,
											max: 2,
									 message: 'State no more than 2 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[a-zA-Z]+$/,
										message: 'State can only contain letters'
									},									
							},
					},

					zip: {
							validators: {
									notEmpty: {
									 message: 'Zip required'
									},
									stringLength: {
											min: 1,
											max: 9,
									 message: 'Zip no more than 9 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[0-9]+$/,
										message: 'Zip can only contain numbers'
									},									
							},
					},

					phone: {
							validators: {
									notEmpty: {
									 message: 'Phone required'
									},
									stringLength: {
											min: 1,
											max: 10,
									 message: 'Phone no more than 10 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[0-9]+$/,
										message: 'Phone can only contain numbers'
									},									
							},
					},

					email: {
							validators: {
									notEmpty: {
									 message: 'Email required'
									},
									stringLength: {
											min: 1,
											max: 100,
									 message: 'Email no more than 100 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/,
										message: 'Must include valid email'
									},									
							},
					},

					Balance: {
							validators: {
									notEmpty: {
									 message: 'Balance required'
									},
									stringLength: {
											min: 1,
											max: 6,
									 message: 'Balance no more than 6 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[0-9\.]+$/,
										message: 'Balance can only contain numbers, and decimal point'
									},									 
							},
					},

					TotalSales: {
							validators: {
									notEmpty: {
									 message: 'Total Sales required'
									},
									stringLength: {
											min: 1,
											max: 6,
									 message: 'Total Sales no more than 6 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[0-9\.]+$/,
										message: 'Total Sales can only contain numbers, and decimal point'
									},									 
							},
					},
					
			}
	});
});
</script>

</body>
</html>
